import {Component} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController} from "ionic-angular";
import {HomePage} from "../home/home";

import {PrincipalProvider} from "../../services/principal";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public email:string;
  public password:string;

  constructor(
    public nav: NavController, 
    public alertCtrl: AlertController, 
    public menu: MenuController, 
    public toastCtrl: ToastController,
    public _principalProvider: PrincipalProvider
  ) {
    this.menu.swipeEnable(false);
  }

  ionViewCanEnter(){
    if(localStorage.getItem('io_login') == '1'){
      this.nav.setRoot(HomePage);
    }
  }

  // login and go to home page
  login() {
    if(this.email !== "" && this.password !== ""){
      
      this._principalProvider.iniciarSesion(this.email, this.password)
      .subscribe(
        res => {
          if(res.error == 0){
            let datos = res.data;
            localStorage.setItem("io_login", "1");
            localStorage.setItem("io_dataUser", JSON.stringify(res.data));
            localStorage.setItem("io_idUser", datos.id);
            localStorage.setItem("io_nameUser", datos.primer_nombre+" "+datos.primer_apellido);
            localStorage.setItem("io_idempresaUser", "1");
            localStorage.setItem("io_emailUser", datos.email);

            this.nav.setRoot(HomePage);

          }else{
            let alert = this.alertCtrl.create({
              title: 'Notificación',
              subTitle: 'Favor revisar credenciales.',
              buttons: ['Cerrar']
            });
            alert.present();
          }
        },
        err => {
          console.log("Error occured");
        }
      );

    }else{
      let alert = this.alertCtrl.create({
        title: 'Notificación',
        subTitle: 'No puede dejar campos vacios.',
        buttons: ['Cerrar']
      });
      alert.present();
    }
  }

  logout() {
    let confirm = this.alertCtrl.create({
      title: "Cerrar Sesi&oacute;n?",
      message: "Seguro desea salir?",
      buttons: [
        {
          text: "Si",
          handler: () => {
            localStorage.removeItem("io_login");
            localStorage.removeItem("io_dataUser");
            localStorage.removeItem("io_idUser");
            localStorage.removeItem("io_nameUser");
            localStorage.removeItem("io_idempresaUser");
            localStorage.removeItem("io_emailUser");

            this.nav.setRoot(LoginPage);
          }
        },
        {
          text: "No"
        }
      ]
    });
    confirm.present();
  }

  forgotPass() {
    let alert = this.alertCtrl.create({
      title: 'Notificación',
      subTitle: 'Este módulo esta en construcción.',
      buttons: ['Cerrar']
    });
    alert.present();
  }

}
