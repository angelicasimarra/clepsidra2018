import {Component} from "@angular/core";
import {NavController, PopoverController} from "ionic-angular";
import {Storage} from '@ionic/storage';

import {SettingsPage} from "../settings/settings";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  public nombre_user: string;
  
  constructor() {
    this.nombre_user = localStorage.getItem("io_nameUser");
  }

  ionViewWillEnter() {
    
  }

}
