import { Component, ViewChild } from "@angular/core";
import { Platform, Nav, AlertController } from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

//Pages
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { SettingsPage } from "../pages/settings/settings";

//Items que tendra nuestro listado de Menu
export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;

  public nombre_user: string;
  public email_user: string;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard,
    public alertCtrl: AlertController, 
  ) {
    this.initializeApp();

    //Menu Aplicacion
    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Configuración', component: SettingsPage, icon: 'cog'},
    ];

    //Informacion del usuario logueado
    this.nombre_user = localStorage.getItem("io_nameUser");
    this.email_user = localStorage.getItem("io_emailUser");

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.keyboard.disableScroll(true);
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  logout() {
    let confirm = this.alertCtrl.create({
      title: "Cerrar Sesi&oacute;n?",
      message: "Seguro desea salir?",
      buttons: [
        {
          text: "Si",
          handler: () => {
            localStorage.removeItem("io_login");
            localStorage.removeItem("io_dataUser");
            localStorage.removeItem("io_idUser");
            localStorage.removeItem("io_nameUser");
            localStorage.removeItem("io_idempresaUser");
            localStorage.removeItem("io_emailUser");
            this.nav.setRoot(LoginPage);
          }
        },
        {
          text: "No"
        }
      ]
    });
    confirm.present();
  }

}
