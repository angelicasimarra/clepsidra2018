import { App, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { LoginPage } from '../../pages/login/login';
import * as sha1 from 'sha1';


@Injectable()
export class HelperPrincipal {

  public key: string = sha1("smart1234info567");
  public UrlWebService: string = "http://demos.smartinfo.com.co/clepsidra_smartinfo/api/helper.php?key="+this.key+"&accion=";
  
  //public loading: any;

  devolverRutaAccion(tipo){
    let ruta;
    switch (tipo) {
      case "configuracion":
        ruta = `${this.UrlWebService}config`;
        break;
      case "login":
        ruta = `${this.UrlWebService}login`;
        break;
      
      default:
        // code...
        break;
    }
    return ruta;
  }

  headerDefault(){
    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");
    return headers;
  }

}

/*import { App, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';

export class HelperPrincipal {
  public UrlWebService: string = "http://smartinfobusiness.com/clepsidra/api/helper.php?accion=";
  public loading: any;

  constructor(
    private alertCtrl: AlertController,
    private app: App,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController
  ) {

  }

  devolverRutaAccion(tipo){
    let ruta;
    switch (tipo) {
      case "configuracion":
        ruta = `${this.UrlWebService}config`;
        break;
      case "login":
        ruta = `${this.UrlWebService}login`;
        break;
      
      default:
        // code...
        break;
    }
    return ruta;
  }

  

  comprobar_login() {
    if (!localStorage.getItem("io_login") || localStorage.getItem("io_login") == "") {
      this.app.getRootNav().setRoot(LoginPage);
    }
  }

  getDatosUsuarioActual() {
    return localStorage.getItem("io_dataUser");
  }

  //Mostrar notificacion
  mostrarNotificacion(
    mensaje: string,
    tipomensaje: string = "default",
    duracion: number = 5000,
    position: string = "top"
  ) {
    // Valores position: "top", "middle", "bottom" 
    // valores clase: rojo, amarillo, azul, default 
    let clase = "toast-" + tipomensaje;
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: duracion,
      cssClass: clase,
      dismissOnPageChange: true,
      position: position,
      showCloseButton: true
    });
    toast.present();
  }

  //Mostrar modal CARGANDO...
  viewLoading(text: string) {
    this.loading = this.loadingCtrl.create({
      content: text
    });
    this.loading.present();
  }

  //Cerrar modal  CARGANDO...
  closeLoading(text: string = "") {
    this.loading.dismiss();
    this.loading.onDidDismiss(() => {
    });
  }

}
*/