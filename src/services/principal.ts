import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import {HelperPrincipal} from '../app/helpers/HelperPrincipal'


@Injectable()
export class PrincipalProvider {
  
  constructor(
    public http: HttpClient, 
    public helperPrincipal: HelperPrincipal) {
    
  }

  iniciarSesion(correo, pass): Observable<any> {
    let headers = this.helperPrincipal.headerDefault();
    return this.http.post(this.helperPrincipal.devolverRutaAccion("login"), {
      email: correo,
      password: pass,
      empresa: 1
    }, {
      headers: headers
    });
  }
  
}